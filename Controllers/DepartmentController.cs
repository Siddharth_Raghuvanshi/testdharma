﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCbyPragim.Models;

namespace MVCbyPragim.Controllers
{
    public class DepartmentController : Controller
    {
        //
        // GET: /Department/

        public ActionResult Index()
        {
            EmployeeContext employeeContext = new EmployeeContext();
            List<Department> department = employeeContext.Department.ToList();
            return View(department);            
        }

    }
}
