﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCbyPragim.Models;
using BusinessLayer;

namespace MVCbyPragim.Controllers
{
    public class EmployeeController : Controller
    {
        //
        // GET: /Employee/


        public ActionResult Index(int departmentId)
        {
            EmployeeContext employeeContext = new EmployeeContext();
            List<Employee> employee = employeeContext.Employees.Where(emp => emp.DepartmentId == departmentId).ToList();
            return View(employee);
        }
        [HttpGet]
        public ActionResult Details(int id)
        {
            EmployeeContext employeeContext = new EmployeeContext();
            //Employee employee = employeeContext.Employees.Single(emp => emp.EmployeeId == id);
            List<Employee> employee = employeeContext.Employees.Where(emp => emp.DepartmentId == id).ToList();
            return View(employee);
        }

        [HttpGet]
        public ActionResult DiplayDetails(int Id)
        {
            EmployeeBusinessLayer employeeBusinessLayer = new EmployeeBusinessLayer();
            //List<EmployeeBLL> employeeBLL = employeeBusinessLayer.Employee.Where(emp => emp.EmployeeId == Id).ToList();
            EmployeeBLL employeeBLL = employeeBusinessLayer.Employee.Single(id => id.EmployeeId == Id);

            return View(employeeBLL);
        }


        public ActionResult EmpDetails()
        {
            EmployeeBusinessLayer employeeBusinessLayer = new EmployeeBusinessLayer();
            IEnumerable<EmployeeBLL> employeeBLL = employeeBusinessLayer.Employee.ToList();
            return View(employeeBLL);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(EmployeeBLL employee)//(FormCollection formCollection)
        {
            //EmployeeBLL employee = new EmployeeBLL();
            //employee.Name = formCollection["Name"];
            //employee.Gender = formCollection["Gender"];
            //employee.City = formCollection["City"];
            //employee.DepartmentId = formCollection["DepartmentId"];

            TryUpdateModel(employee);
            if (ModelState.IsValid)
            {
                EmployeeBusinessLayer employeeBusinessLayer = new EmployeeBusinessLayer();
                employeeBusinessLayer.AddEmployee(employee);

                return RedirectToAction("EmpDetails");
            }

            //foreach (string key in formCollection.AllKeys)
            //{
            //    Response.Write("Key = " + key + " ");
            //    Response.Write(formCollection[key]);
            //    Response.Write("<br />");
            //}
            return View();
        }

        [HttpGet]
        public ActionResult Edit(int Id)
        {
            EmployeeBusinessLayer employeeBusinessLayer = new EmployeeBusinessLayer();
            EmployeeBLL employeeBLL = employeeBusinessLayer.Employee.Single(emp => emp.EmployeeId == Id);
            return View(employeeBLL);
        }

        [HttpPost]
        [ActionName("Edit")]
        public ActionResult Edit_Post(int Id)
        {
            EmployeeBusinessLayer employeeBusinessLayer = new EmployeeBusinessLayer();
            EmployeeBLL employee = employeeBusinessLayer.Employee.Single(id => id.EmployeeId == Id);
            UpdateModel(employee, new string[] { "City", "DepartmentId", "EmployeeId", "Gender" });
            if (ModelState.IsValid)
            {
                employeeBusinessLayer.UpdateEmployee(employee);
                return RedirectToAction("EmpDetails");
            }
            return View();

            //TryUpdateModel(empBLL);
            //if (ModelState.IsValid)
            //{
            //    EmployeeBusinessLayer employeeBusinessLayer = new EmployeeBusinessLayer();
            //    employeeBusinessLayer.UpdateEmployee(empBLL);
            //    return RedirectToAction("EmpDetails");
            //}
            //return View();
        }
        [HttpPost]
        public ActionResult DeleteRecord(int id)
        {
            if (ModelState.IsValid)
            {
                EmployeeBusinessLayer employeeBusinessLayer = new EmployeeBusinessLayer();
                employeeBusinessLayer.DeleteEmployeeRecord(id);
                return RedirectToAction("EmpDetails");

            }
            return View();
        }
    }
}
